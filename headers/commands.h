#ifndef LAB1_COMMANDS_H
#define LAB1_COMMANDS_H
#include "string.h"
#include "limits.h"
enum command_types {
    LS = 0,
    PWD = 1,
    CD = 2,
    CP = 3,
    QUIT = 4,
    HELP = 5
};

struct commands {
    enum command_types type;
    char* firstArgument;
    char* secondArgument;
};


struct commands parse_command(char* string);

#endif
